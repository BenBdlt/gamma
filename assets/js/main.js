(function($) {
    'use strict';

    $("a.logo-link").on('click', function() {
        $('html, body').stop().animate({scrollTop:0}, 500);
    });
    $("a.agence-link").on('click', function() {
        $('html, body').stop().animate({scrollTop:$('#home-mission').offset().top-60}, 500);
    });
    $("a.techno-link").on('click', function() {
        $('html, body').stop().animate({scrollTop:$('#home-job').offset().top-60}, 500);
    });
    $("a.membre-link").on('click', function() {
        $('html, body').stop().animate({scrollTop:$('#home-role').offset().top-60}, 500);
    });

    //FONCTION FAIT A LA MAIN (j'en suis pas peu fier j'ai galeré msk)
    var $bool = true;
    $('a.burger').on('click', function () {
        if ($bool) {
            $('#imgmenu').removeClass('rotate2');
            $('#imgmenu').addClass('rotate');
            $('#imgmenu').animate({}, 1000, function () {
            });
            $('div.position-fixed').removeClass('slide2');
            $('div.position-fixed').addClass('slide');
            $('div.position-fixed').animate({}, 1000, function (){
            });
            $('div.searching').css('display', 'none');
            $('li.nav-item').css('display', 'none');
            $('img.logo-header').css('display', 'none');
            $('section.section-padding').removeClass('col-10');
            $('section.section-padding').addClass('col-12');
            $('section.section-padding').animate({}, 1000, function () {
            });
            $bool = false;
        } else {
            $('#imgmenu').removeClass('rotate');
            $('#imgmenu').addClass('rotate2');
            $('#imgmenu').animate({}, 1000, function () {
            });
            $('div.position-fixed').removeClass('slide');
            $('div.position-fixed').addClass('slide2');
            $('div.position-fixed').animate({}, 1200, function (){
            });
            $('div.searching').css('display', 'flex');
            $('li.nav-item').css('display', 'flex');
            $('img.logo-header').css('display', 'flex');
            $('section.section-padding').removeClass('col-12');
            $('section.section-padding').addClass('col-10');
            $('section.section-padding').animate({}, 1000, function () {
            });
            $bool = true;
        }
    });

    // VALIDE LIKE
    var $liked_count = 1;
    var $id = 2;
    var $bool = true;
    var $return = 0; // 0 = pas de like ; 1 = like img-1 ; 2 = like img-2 ; 3 dislike img-1 ; 4 dislike img-2 ;
    $('button.valid').on('click', function () {
        //LE STOCK EST TIL ECOULER ?
        if ($id < 9) {
            // ON FAIS UN ECHANGE ENTRE LES IMG POUR PERMETTRE UN SLIDE FLUIDE
            if ($bool) {
                $('.list-like').prepend('<div id="'+ $id +'" class="prod-like row m-0"><img id="" src="assets/image/prod'+ ($id - 1) +'.PNG" class="img-list" alt="actual match"><div class="text-prod-like"><h5>Nom du modèle</h5><p>Nike</p><p>150€</p><button class="redirect">Voir sur le site</button></div></div>');
                $('#main-img-2').removeClass('slide-in-blurred-right');
                $('#main-img-2').removeClass('slide-in-blurred-left');
                $('#main-img-1').addClass('slide-in-blurred-right');
                $('#main-img-2').attr('src', 'assets/image/prod' + $id + '.PNG').delay(500).fadeIn(400);
                $('#main-img-1').delay(100).fadeOut(400);
                $bool = false;
                $return = 1;
            }
            else {
                $('.list-like').prepend('<div id="'+ $id +'" class="prod-like row m-0"><img id="" src="assets/image/prod'+ ($id - 1) +'.PNG" class="img-list" alt="actual match"><div class="text-prod-like"><h5>Nom du modèle</h5><p>Nike</p><p>150€</p><button class="redirect">Voir sur le site</button></div></div>');
                $('#main-img-1').removeClass('slide-in-blurred-right');
                $('#main-img-1').removeClass('slide-in-blurred-left');
                $('#main-img-2').addClass('slide-in-blurred-right');
                $('#main-img-1').attr('src', 'assets/image/prod' + $id + '.PNG').delay(500).fadeIn(400);
                $('#main-img-2').delay(100).fadeOut(400);
                $bool = true;
                $return = 2;
            }
            $id += 1;
        }
        else {
            $('.list-like').prepend('<div class="prod-like row m-0"><img id="" src="assets/image/prod'+ ($id - 1) +'.PNG" class="img-list" alt="actual match"><div class="text-prod-like"><h5>Nom du modèle</h5><p>Nike</p><p>150€</p><button class="redirect">Voir sur le site</button></div></div>');
            $('#main-img-1').addClass('slide-in-blurred-right');
            $('#main-img-2').addClass('slide-in-blurred-right');
            $('h1.endstock').delay(500).fadeIn(600);
            $('#main-img-1').fadeOut(500);
            $('#main-img-2').fadeOut(500);
            $('button.valid').prop('disabled',true);
            $('button.refuse').prop('disabled',true);
        }
        //SUITE DE LA FONCTION POUR LE COUNT DU MENU
        if ($liked_count === 1) {
            $('.counter').show(350);
            $('.count-nb').text($liked_count);
            $liked_count += 1;
        }
        else {
            $('.count-nb').text($liked_count);
            $liked_count += 1;
        }
    });

    // REFUSE DISLIKE
    $('button.refuse').on('click', function () {
        //LE STOCK EST TIL ECOULER ?
        if ($id < 9) {
            // ON FAIS UN ECHANGE ENTRE LES IMG POUR PERMETTRE UN SLIDE FLUIDE
            if ($bool) {
                $('#main-img-2').removeClass('slide-in-blurred-left');
                $('#main-img-2').removeClass('slide-in-blurred-right');
                $('#main-img-1').addClass('slide-in-blurred-left');
                $('#main-img-2').attr('src', 'assets/image/prod' + $id + '.PNG').delay(500).fadeIn(400);
                $('#main-img-1').delay(100).fadeOut(400);
                $bool = false;
                $return = 3;
            }
            else {
                $('#main-img-1').removeClass('slide-in-blurred-left');
                $('#main-img-1').removeClass('slide-in-blurred-right');
                $('#main-img-2').addClass('slide-in-blurred-left');
                $('#main-img-1').attr('src', 'assets/image/prod' + $id + '.PNG').delay(500).fadeIn(400);
                $('#main-img-2').delay(100).fadeOut(400);
                $bool = true;
                $return = 4;
            }
            $id += 1;
        }
        else {
            $('#main-img-1').addClass('slide-in-blurred-left');
            $('#main-img-2').addClass('slide-in-blurred-left');
            $('h1.endstock').delay(500).fadeIn(600);
            $('#main-img-1').fadeOut(500);
            $('#main-img-2').fadeOut(500);
            $('button.valid').prop('disabled',true);
            $('button.refuse').prop('disabled',true);
        }
    });

    //BOUTON MENU
    var $open = true;
    $('button.menu').on('click', function () {
        if($open) {
            $('.menu-open').show(400);
            $('.menu-open').addClass('slide3');
            $('.menu-open').animate({}, 1000, function (){
            });
            $open = false;
        }
        else {
            $('.menu-open').hide(400);
            $('.menu-open').removeClass('slide3');
            $('.menu-open').animate({}, 1000, function (){
            });
            $open = true;
        }
    });

    // BOUTON BACK
    $('button.back').on('click', function () {
        /**if ($return === 0) {
        }**/
        if ($return === 1) {
            $('#'+$id+'').remove();
            $('#main-img-2').fadeOut(100);
            $('#main-img-1').fadeIn(100);
            $('#main-img-1').removeClass('slide-in-blurred-right');
        }
        /**
        if ($return === 2) {
            alert($return);
        }
        if ($return === 3) {
            alert($return);
        }
        if ($return === 4) {
            alert($return);
        **/
    });



    // A DEVELOPPER AFIN DE FAIRE UNE BARRE DE RECHERCHE DE MOT CLE DU SITE
    /*$(document).ready(function(){
        $("inputSearch").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });
    });*/
})(jQuery);