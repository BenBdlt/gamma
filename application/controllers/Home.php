<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model', 'userManager');

    }

    public
    function index()
    {
        // Chargement des CSS
        $this->data['css'] = $this->layout->add_css(array(
            'assets/plugins/bootstrap/css/bootstrap.min',
            'assets/css/styles',
            'assets/css/style',
        ));
        // Chargement des JS
        $this->data['js'] = $this->layout->add_js(array(
            'assets/plugins/jquery-3.3.1.min',
            'assets/plugins/bootstrap/js/bootstrap.min',
            'assets/js/main'
        ));

        // Chargement de la vue
        $this->data['subview'] = 'front_office/home/main';

        //HEADER
        $this->data['header'] = true;

        // FOOTER
        $this->data['footer'] = true;

        $this->load->view('components_home/main', $this->data);
    }


}