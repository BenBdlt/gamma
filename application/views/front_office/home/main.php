<section class="row p-0 m-0 col-2 zindex-2">
    <div class="col-2 p-0 position-fixed">
        <nav class="navbar vh-100 align-items-start pt-3" style="background-color: #3D6380">
            <a class="burger">
                <img src="<?= base_url() ?>assets/image/closemenu.png" class="imgmenu" id="imgmenu"/>
            </a>
            <ul class="nav flex-column h-75">
                <a class="logo-link">
                    <img src="<?= base_url() ?>assets/image/GAMMA_logo.png" alt="" class="logo-header">
                </a>
                <li class="nav-item pt-3 mt-3">
                    <a class="nav-link btn-outline-light itemnav agence-link" href="#home-bio">
                        <img class="imgnav" src="<?= base_url() ?>assets/image/header_agence.png"/>
                        <p class="m-0" style="padding-top: 0.4rem">Présentation de l'agence</p>
                    </a>
                </li>
                <li class="nav-item pt-2">
                    <a class="nav-link btn-outline-light itemnav techno-link" href="#home-mission">
                        <img class="imgnav" src="<?= base_url() ?>assets/image/header_techno.png"/>
                        <p class="m-0">Technologies utilisées</p>
                    </a>
                </li>
                <li class="nav-item pt-2">
                    <a class="nav-link btn-outline-light itemnav membre-link" href="#home-job">
                        <img class="imgnav" src="<?= base_url() ?>assets/image/header_team.png"/>
                        <p class="m-0" style="padding-top: 0.6rem">Les membres de l'équipe</p>
                    </a>
                </li>
                <li class="nav-item mt-auto">
                    <a class="nav-link btn-outline-light itemnav membre-link" href="<?= base_url('dev') ?>">
<!--                        <img class="imgnav" src="--><?//= base_url() ?><!--assets/image/header_team.png"/>-->
                        <p class="m-0" style="">Fonctionnalités</p>
                    </a>
                </li>
            </ul>
        </nav>
        <div class="area" >
            <ul class="circles">
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </div>
    </div>

</section>
<section class="m-0 section-padding col-10">
    <div class="titre container-min vh-100 p-0">
<!--        <img src="--><?//= base_url() ?><!--assets/image/GAMMA_logo.png" alt="" class="logo-gamma">-->
        <h1 class="titrealpha">Présentation de l'équipe Gamma <br> Campus Track 2021</h1>
        <video src="<?= base_url() ?>assets/image/video/gamma_video.mp4" autoplay class="logo-gamma"></video>
    </div>


    <div class="textbox rbox container" id="home-mission">
        <h2>Présentation de l'agence</h2>
        <p id="textleft">
            Gamma est une agence numérique au service de l'innovation. Notre ADN est d'apporter une nouvelle approche d'un produit via la technologie.
            Gamma est une jeune entreprise composée de 15 étudiants issues de différents univers.
            Nos multiples casquettes telles que la modélisation 3D, le développement d'application ou notre expertise
            dans le monde du réseau et de la sécurité informatique nous permet d'avoir différents points de vues et de s'adapter à la demande.
            Chaque client est unique, et nous porte dans une aventure nouvelle
        </p>
    </div>

    <div class="textbox container" id="home-job">
        <h2>Technonologies utilisées</h2>
        <div class="row">
            <div class="w-25 mx-auto">
                <div class="circularimg cercleCA">
                    <img src="<?= base_url() ?>assets/image/threejs-logo.jpg" />
                </div>
                <p class="titlename">ThreeJS</p>
            </div>
            <div class="w-25 mx-auto">
                <div class="circularimg cercleCA">
                    <img src="<?= base_url() ?>assets/image/blender-logo.png" />
                </div>
                <p class="titlename">Blender</p>
            </div>
            <div class="w-25 mx-auto">
                <div class="circularimg cercleCA">
                    <img src="<?= base_url() ?>assets/image/vuforia-logo.jpg" />
                </div>
                <p class="titlename">Vuforia</p>
            </div>
            <div class="w-25 mx-auto">
                <div class="circularimg cercleCA">
                    <img src="<?= base_url() ?>assets/image/csharp-logo.png" />
                </div>
                <p class="titlename">C#</p>
            </div>
        </div>
    </div>

    <div class="textbox rbox container " id="home-role">
        <h2>Les membres de l'équipe</h2>
        <div class="row">
            <div class="w-25 mx-auto">
                <div class="circularimg cerclename">
                    <img src="<?= base_url() ?>assets/image/gamma-cyril.jpg" />
                </div>
                <h5 class="titlename">Cyril Dintheer</h5>
                <h6>M2 Switch IT Dev</h6>
                <p class="desc">"Si c'est ça être sage, alors je préfère rester un idiot pour le restant de mes jours !"</p>
            </div>

            <div class="w-25 mx-auto">
                <div class="circularimg cerclename">
                    <img src="<?= base_url() ?>assets/image/gamma-thibault.jpg" />
                </div>
                <h5 class="titlename">Thibault Ledanois</h5>
                <h6>B3 Switch IT Dev</h6>
                <p class="desc">"Le désir et le rêve sont à la base de l'accomplissement de toutes choses."</p>
            </div>

            <div class="w-25 mx-auto">
                <div class="circularimg cerclename">
                    <img src="<?= base_url() ?>assets/image/img_profil.jpg" />
                </div>
                <h5 class="titlename">Benjamin Bordelet</h5>
                <h6>B2 Switch IT Dev</h6>
                <p class="desc">"Agis comme un faucon agile"</p>
            </div>
        </div>
        <div class="row">
            <div class="w-25 mx-auto">
                <div class="circularimg cerclename">
                    <img src="<?= base_url() ?>assets/image/gamma-thomas.png" />
                </div>
                <h5 class="titlename">Thomas Le Merdy</h5>
                <h6>B1 E-sport</h6>
                <p class="desc">"J'aime l'été, les animés, les jeux et les copains"</p>
            </div>

            <div class="w-25 mx-auto">
                <div class="circularimg cerclename">
                    <img src="<?= base_url() ?>assets/image/gamma-jeremie-b.png" />
                </div>
                <h5 class="titlename">Jérémie Buton</h5>
                <h6>B1 NYAD</h6>
                <p class="desc">"J'aime les jeux et manger, le stream c'est bien aussi (KoBi_TV)"</p>
            </div>

            <div class="w-25 mx-auto">
                <div class="circularimg cerclename">
                    <img src="<?= base_url() ?>assets/image/gamma-tom.jpg" />
                </div>
                <h5 class="titlename">Tom Tessier</h5>
                <h6>B2 E-sport</h6>
                <p class="desc">"Le sport est bien affaire de frissons"</p>
            </div>
        </div>
        <div class="row">
            <div class="w-25 mx-auto">
                <div class="circularimg cerclename">
                    <img src="<?= base_url() ?>assets/image/gamma-rodolphe.jpg" />
                </div>
                <h5 class="titlename">Rodolphe Malichecq</h5>
                <h6>B1 Sport 3D</h6>
                <p class="desc">"Créateur d'univers !"</p>
            </div>

            <div class="w-25 mx-auto">
                <div class="circularimg cerclename">
                    <img src="<?= base_url() ?>assets/image/gamma-florian.jpg" />
                </div>
                <h5 class="titlename">Florian Garcia</h5>
                <h6>B1 Sport 3D</h6>
                <p class="desc">"Créateur depuis 2001"</p>
            </div>

            <div class="w-25 mx-auto">
                <div class="circularimg cerclename">
                    <img src="<?= base_url() ?>assets/image/gamma-hassan.png" />
                </div>
                <h5 class="titlename">Hassan Touka</h5>
                <h6>B3 Switch IT Dev</h6>
                <p class="desc">"Si vous voulez que vos rêves se réalisent, ne dormez pas."</p>
            </div>
        </div>
        <div class="row">
            <div class="w-25 mx-auto">
                <div class="circularimg cerclename">
                    <img src="<?= base_url() ?>assets/image/gamma-adrien.jpg" />
                </div>
                <h5 class="titlename">Adrien Meunier</h5>
                <h6>B2 Switch IT Dev</h6>
                <p class="desc">"Pourquoi pleurer quand on peut rire"</p>
            </div>

            <div class="w-25 mx-auto">
                <div class="circularimg cerclename">
                    <img src="<?= base_url() ?>assets/image/gamma-guillaume.png" />
                </div>
                <h5 class="titlename">Guillaume Charpentier</h5>
                <h6>B3 Switch IT Dev</h6>
                <p class="desc">"ravi d'intégrer une équipe motivée pour réaliser un projet concret et répondre à des annonceurs de qualité !"</p>
            </div>

            <div class="w-25 mx-auto">
                <div class="circularimg cerclename">
                    <img src="<?= base_url() ?>assets/image/gamma-romanne.jpg" />
                </div>
                <h5 class="titlename">Romanne Poisson</h5>
                <h6>B2 Spot 3D</h6>
                <p class="desc">"La vie n’a aucun sens. Et pourtant personne n’a envie de mourir, pas vrai ?"</p>
            </div>
        </div>
        <div class="row">
            <div class="w-25 mx-auto">
                <div class="circularimg cerclename">
                    <img src="<?= base_url() ?>assets/image/gamma-jeremy-barré.jpg" />
                </div>
                <h5 class="titlename">Jérémy Barré</h5>
                <h6>B1 Switch IT</h6>
                <p class="desc">"On a rien sans rien"</p>
            </div>

            <div class="w-25 mx-auto">
                <div class="circularimg cerclename">
                    <img src="<?= base_url() ?>assets/image/gamma-maxime.jpg" />
                </div>
                <h5 class="titlename">Maxime Bujeau</h5>
                <h6>B3 Switch IT OPS</h6>
                <p class="desc">"Mens sana, in corpore sano"</p>
            </div>

            <div class="w-25 mx-auto">
                <div class="circularimg cerclename">
                    <img src="<?= base_url() ?>assets/image/gamma-alix.jpg" />
                </div>
                <h5 class="titlename">Alix</h5>
                <h6>B2 Switch IT OPS</h6>
                <p class="desc">"J'adore Linux, je déteste Windows"</p>
            </div>
        </div>
    </div>
</section