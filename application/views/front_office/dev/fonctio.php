<section class="shoe-match vh-100">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="actual-match">
                    <img class="logo-courir" src="<?= base_url() ?>assets/image/courir_logo.png" alt="">
                    <!--<h5 class="match-txt">Alors ça match ?</h5>-->
                    <h1 class="endstock">Plus de chaussure en stock !</h1>
                    <img id="main-img-1" src="<?= base_url() ?>assets/image/prod1.PNG" class="" alt="actual match">
                    <img id="main-img-2" src="<?= base_url() ?>assets/image/prod1.PNG" class="" alt="actual match">
<!--                    <img src="--><?//= base_url() ?><!--assets/image/nikeair95.PNG" class="circle-ripple" alt="actual match">-->
<!--                    <img src="--><?//= base_url() ?><!--assets/image/niketn.jpg" class="circle-ripple" alt="actual match">-->
                    <!--<div class="like-anim"><img src="<?= base_url() ?>assets/image/btn_heart.png" alt=""></div>-->

                    <div class="text-shoe">
                        <p class="modele">Nom du modèle</p>
                        <p class="brand">Nike</p>
                    </div>
                    <div class="choice">
                        <button class="btn back">
                            <img src="<?= base_url() ?>assets/image/go-back-arrow.png">
                        </button>
                        <button class="btn refuse">
                            <img src="<?= base_url() ?>assets/image/btn_close.png">
                        </button>
                        <!--<button class="btn superlike">
                            <img src="<?= base_url() ?>assets/image/btn_star.png">
                        </button>-->
                        <button class="btn valid shadow-lg">
                            <img src="<?= base_url() ?>assets/image/btn_heart.png">
                        </button>
                        <button class="btn menu">
                            <div class="counter"><p class="count-nb"></p></div>
                            <img src="<?= base_url() ?>assets/image/btn_menu.png">
                        </button>
                    </div>
                </div>
            </div>
            <div class="menu-open vh-100">
                <div class="list-like">
                </div>
            </div>
        </div>
    </div>
</section>